package com.sigapi.sigapi_rev.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Peta {
    @SerializedName("fasilitas")
    @Expose
    private List<Fasilitas> fasilitas = null;

    @SerializedName("krb")
    @Expose
    private List<Krb> krb = null;

    public List<Fasilitas> getFasilitas(){ return fasilitas; }
    public void setFasilitas(List<Fasilitas> fasilitas){ this.fasilitas  = fasilitas; }

    public List<Krb> getKrb(){ return krb; }
    public void setKrb(List<Krb> krb){ this.krb = krb; }
}
