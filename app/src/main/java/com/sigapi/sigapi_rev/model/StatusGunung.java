package com.sigapi.sigapi_rev.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StatusGunung {

    @SerializedName("gunung")
    @Expose
    private String gunung;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("tanggal")
    @Expose
    private String tgl;

    public StatusGunung(String gunung,String status, String tgl ){
        this.gunung = gunung;
        this.status = status;
        this.tgl = tgl;
    }

    public String getGunung(){ return gunung; }
    public void setGunung(String gunung){ this.gunung = gunung; }

    public String getStatus(){ return status; }
    public void setStatus(String status){ this.status = status; }

    public String getTanggal(){ return tgl; }
    public void setTanggal(String tgl){ this.tgl = tgl; }
}
