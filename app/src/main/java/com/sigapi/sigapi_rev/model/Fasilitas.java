package com.sigapi.sigapi_rev.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Fasilitas {
    @SerializedName("id_fasilitas")
    @Expose
    private Integer idFasilitas;

    @SerializedName("nama_fasilitas")
    @Expose
    private String nama;

    @SerializedName("alamat_fasilitas")
    @Expose
    private String alamat;

    @SerializedName("longitude")
    @Expose
    private String longitude;

    @SerializedName("latitude")
    @Expose
    private String latitude;

    @SerializedName("id_jenis_fasilitas")
    @Expose
    private Integer idJenisFasilitas;

    @SerializedName("jenis_fasilitas")
    @Expose
    private String jenisFasilitas;

    @SerializedName("gunung")
    @Expose
    private String gunung;

    public Fasilitas(Integer idFasilitas, String nama, String alamat, String longitude, String latitude, Integer idJenisFasilitas,
        String jenisFasilitas, String gunung){
        this.idFasilitas = idFasilitas;
        this.nama = nama;
        this.alamat = alamat;
        this.longitude = longitude;
        this.latitude = latitude;
        this.idJenisFasilitas = idJenisFasilitas;
        this.jenisFasilitas = jenisFasilitas;
        this.gunung = gunung;
    }

    public Integer getIdFasilitas(){ return idFasilitas; }
    public void setIdFasilitas(Integer idFasilitas){ this.idFasilitas  = idFasilitas; }

    public String getNamaFasilitas(){ return nama; }
    public void setNamaFasilitas(String nama){ this.nama  = nama; }

    public String getAlamatFasilitas(){ return alamat; }
    public void setAlamatFasilitas(String alamat){ this.alamat  = alamat; }

    public String getLongitudeFasilitas(){ return longitude; }
    public void setLongitudeFasilitas(String longitude){ this.longitude  = longitude; }

    public String getLatitudeFasilitas(){ return latitude; }
    public void setLatitudeFasilitas(String latitude){ this.latitude  = latitude; }

    public String getJenisFasilitas(){ return jenisFasilitas; }
    public void setJenisFasilitas(String jenisFasilitas){ this.jenisFasilitas  = jenisFasilitas; }

    public Integer getIdJenisFasilitas(){ return idJenisFasilitas; }
    public void setIdJenisFasilitas(Integer idJenisFasilitas){ this.idJenisFasilitas  = idJenisFasilitas; }

    public String getGunung(){ return gunung; }
    public void setGunung(String gunung){ this.gunung  = gunung; }
    
}
