package com.sigapi.sigapi_rev.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Krb {
    @SerializedName("id_krb")
    @Expose
    private Integer idKrb;

    @SerializedName("file_krb")
    @Expose
    private String file;

    @SerializedName("id_jenis_krb")
    @Expose
    private Integer idJenisKrb;

    @SerializedName("jenis_krb")
    @Expose
    private String jenisKrb;

    @SerializedName("gunung")
    @Expose
    private String gunung;

    public Krb(Integer idKrb, String file, Integer idJenisKrb,
                     String jenisKrb, String gunung){
        this.idKrb = idKrb;
        this.file = file;
        this.idJenisKrb = idJenisKrb;
        this.jenisKrb = jenisKrb;
        this.gunung = gunung;
    }

    public Integer getIdKrb(){ return idKrb; }
    public void setIdKrb(Integer idKrb){ this.idKrb  = idKrb; }

    public String getFileKrb(){ return file; }
    public void setFileKrb(String file){ this.file  = file; }

    public String getJenisKrb(){ return jenisKrb; }
    public void setJenisKrb(String jenisKrb){ this.jenisKrb  = jenisKrb; }

    public Integer getIdJenisKrb(){ return idJenisKrb; }
    public void setIdJenisKrb(Integer idJenisKrb){ this.idJenisKrb  = idJenisKrb; }

    public String getGunung(){ return gunung; }
    public void setGunung(String gunung){ this.gunung  = gunung; }
}
