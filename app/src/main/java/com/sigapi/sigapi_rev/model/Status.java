package com.sigapi.sigapi_rev.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Status {

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("status_gunung")
    @Expose
    private List<StatusGunung> statusGunungList;

    public Status(String status, List<StatusGunung> statusGunungList){
        this.status = status;
        this.statusGunungList = statusGunungList;
    }

    public String getStatus(){ return status; }
    public void setStatus(String status){ this.status = status; }

    public List<StatusGunung> getStatusGunung(){ return statusGunungList; }
    public void setStatusGunung(List<StatusGunung> statusGunungList){ this.statusGunungList = statusGunungList; }
}
