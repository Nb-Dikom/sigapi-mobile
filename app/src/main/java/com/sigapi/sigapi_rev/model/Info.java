package com.sigapi.sigapi_rev.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Info {
    @SerializedName("id_info")
    @Expose
    private Integer idInfo;

    @SerializedName("nama_info")
    @Expose
    private String nama;

    @SerializedName("konten")
    @Expose
    private String konten;

    @SerializedName("tanggal_info")
    @Expose
    private String tgl;

    public Info (Integer id_info, String nama, String konten, String tgl){
        this.idInfo = id_info;
        this.nama = nama;
        this.konten = konten;
        this.tgl = tgl;
    }

    public Integer getIdInfo() {
        return idInfo;
    }
    public void setIdInfo(Integer idInfo){ this.idInfo = idInfo; }

    public String getNamaInfo(){ return nama; }
    public void setNama(String nama){ this.nama = nama; }

    public String getKontenInfo(){ return konten; }
    public void setKonten(String konten){ this.konten = konten;}

    public String getTglInfo(){ return tgl; }
    public void setTgl(String tgl){ this.tgl = tgl;}
}
