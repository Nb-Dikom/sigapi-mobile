package com.sigapi.sigapi_rev.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pengamatan {
    @SerializedName("id_pengamatan")
    @Expose
    private Integer idPengamatan;

    @SerializedName("nama_pengamatan")
    @Expose
    private String nama;

    @SerializedName("foto_pengamatan")
    @Expose
    private String file;

    @SerializedName("tanggal_pengamatan")
    @Expose
    private String tgl;

    public Pengamatan(Integer idPengamatan, String nama, String file, String tgl){
        this.idPengamatan = idPengamatan;
        this.nama = nama;
        this.file = file;
        this.tgl = tgl;
    }

    public Integer getIdPengamatan(){ return idPengamatan; }
    public void setIdPengamatan(Integer idPengamatan){ this.idPengamatan  = idPengamatan; }

    public String getNamaPengamatan(){ return nama; }
    public void setNamaPengamatan(String nama){ this.nama = nama; }

    public String getFilePengamatan(){ return file; }
    public void setFilePengamatan(String file){ this.file = file;}

    public String getTglPengamatan(){ return tgl; }
    public void setTgl(String tgl){ this.tgl = tgl;}
}
