package com.sigapi.sigapi_rev.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sigapi.sigapi_rev.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class DetailPengamatanActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pengamatan);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        TextView nama = (TextView) findViewById(R.id.nama);
        ImageView foto = (ImageView) findViewById(R.id.foto);
        TextView tanggal = (TextView) findViewById(R.id.tanggal);

        ActionBar back = getSupportActionBar();
        back.setDisplayHomeAsUpEnabled(true);

        nama.setText(getIntent().getStringExtra("nama"));
        tanggal.setText(getIntent().getStringExtra("tanggal"));

        String image = "http://192.168.1.101/sigapi/public/upload/pengamatan/" +getIntent().getStringExtra("foto");
//        String image = "http://192.168.43.84/sigapi/public/upload/pengamatan/" +getIntent().getStringExtra("foto");
//        String image = "http://192.168.0.107/sigapi/public/upload/pengamatan/" +getIntent().getStringExtra("foto");

        if(getIntent().getStringExtra("foto") == null){
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(foto.getWidth(), 0);
            foto.setLayoutParams(params);
        }
        else{
            final ProgressDialog progressDialog = new ProgressDialog(DetailPengamatanActivity.this);
            progressDialog.setMessage("Harap tunggu ...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            Picasso.with(this)
                    .load(image)
                    .error(R.drawable.noimagefound)
                    .placeholder(R.drawable.noimagefound)
                    .into(foto, new Callback() {
                        @Override
                        public void onSuccess() {
                            progressDialog.dismiss();
                        }

                        @Override
                        public void onError() {
                            progressDialog.dismiss();
                        }
                    });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public static void Detail(Activity activity, Integer id, String nama, String foto, String tanggal){

        Intent intent = new Intent(activity, DetailPengamatanActivity.class);
        intent.putExtra("id", id);
        intent.putExtra("nama", nama);
        intent.putExtra("foto", foto);
        intent.putExtra("tanggal", tanggal);

        activity.startActivity(intent);
    }
}
