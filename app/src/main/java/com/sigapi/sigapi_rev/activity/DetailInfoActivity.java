package com.sigapi.sigapi_rev.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.widget.TextView;

import com.sigapi.sigapi_rev.R;

import org.sufficientlysecure.htmltextview.HtmlHttpImageGetter;
import org.sufficientlysecure.htmltextview.HtmlTextView;

public class DetailInfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_info);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        TextView nama = (TextView) findViewById(R.id.nama);
        HtmlTextView konten = (HtmlTextView) findViewById(R.id.konten);
        TextView tanggal = (TextView) findViewById(R.id.tanggal);

        ActionBar back = getSupportActionBar();
        back.setDisplayHomeAsUpEnabled(true);

        nama.setText(getIntent().getStringExtra("nama"));
        konten.setHtml(getIntent().getStringExtra("konten"), new HtmlHttpImageGetter(konten));
        tanggal.setText(getIntent().getStringExtra("tanggal"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public static void Detail(Activity activity, Integer id, String nama, String konten, String tanggal){

        Intent intent = new Intent(activity, DetailInfoActivity.class);
        intent.putExtra("id", id);
        intent.putExtra("nama", nama);
        intent.putExtra("konten", konten);
        intent.putExtra("tanggal", tanggal);

        activity.startActivity(intent);
    }
}
