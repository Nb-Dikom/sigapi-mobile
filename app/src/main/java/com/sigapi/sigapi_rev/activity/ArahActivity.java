package com.sigapi.sigapi_rev.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.esri.arcgisruntime.ArcGISRuntimeEnvironment;
import com.esri.arcgisruntime.UnitSystem;
import com.esri.arcgisruntime.concurrent.ListenableFuture;
import com.esri.arcgisruntime.geometry.Point;
import com.esri.arcgisruntime.geometry.SpatialReferences;
import com.esri.arcgisruntime.mapping.ArcGISMap;
import com.esri.arcgisruntime.mapping.Basemap;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;
import com.esri.arcgisruntime.mapping.view.MapView;
import com.esri.arcgisruntime.symbology.PictureMarkerSymbol;
import com.esri.arcgisruntime.symbology.SimpleLineSymbol;
import com.esri.arcgisruntime.tasks.networkanalysis.DirectionManeuver;
import com.esri.arcgisruntime.tasks.networkanalysis.DirectionManeuverType;
import com.esri.arcgisruntime.tasks.networkanalysis.DirectionsStyle;
import com.esri.arcgisruntime.tasks.networkanalysis.Route;
import com.esri.arcgisruntime.tasks.networkanalysis.RouteParameters;
import com.esri.arcgisruntime.tasks.networkanalysis.RouteResult;
import com.esri.arcgisruntime.tasks.networkanalysis.RouteTask;
import com.esri.arcgisruntime.tasks.networkanalysis.RouteTaskInfo;
import com.esri.arcgisruntime.tasks.networkanalysis.Stop;
import com.esri.arcgisruntime.tasks.networkanalysis.TravelMode;
import com.sigapi.sigapi_rev.R;
import com.sigapi.sigapi_rev.util.GPSTracker;

import java.util.ArrayList;
import java.util.List;

public class ArahActivity extends AppCompatActivity {
    private static final String TAG = "LOG";
    private Double startLongitude, startLatitude;
    private Double endLongitude, endLatitude;

    private MapView mapView;
    private RouteParameters routeParameters;
    private Route route;
    private SimpleLineSymbol lineSymbol;
    private GraphicsOverlay graphicsOverlay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arah);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar back = getSupportActionBar();
        back.setDisplayHomeAsUpEnabled(true);

        GPSTracker gpsTracker = new GPSTracker(this);
        startLongitude = gpsTracker.getLongitude();
        startLatitude = gpsTracker.getLatitude();

        endLongitude = Double.parseDouble(getIntent().getStringExtra("longitude"));
        endLatitude = Double.parseDouble(getIntent().getStringExtra("latitude"));

//        startLongitude = 106.688499;
//        startLatitude = -6.154715;
//        endLongitude = 106.699244;
//        endLatitude = -6.144598;
//        endLongitude = 106.759121;
//        endLatitude = -6.311072;

        ArcGISRuntimeEnvironment.setLicense("runtimelite,1000,rud6300666834,none,6PB3LNBHPBHDLMZ59175");
        ArcGISMap map = new ArcGISMap(Basemap.Type.NAVIGATION_VECTOR, startLatitude, startLongitude, 16);

        mapView = (MapView) findViewById(R.id.mapView);
        mapView.setMap(map);

        setupSymbols();
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Harap tunggu ...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        final RouteTask routeTask = new RouteTask( getApplicationContext(), getString(R.string.route_service));
        final ListenableFuture<RouteParameters> listenableFuture = routeTask.createDefaultParametersAsync();
        listenableFuture.addDoneListener(new Runnable() {
            @Override
            public void run() {
                try {
                    if (listenableFuture.isDone()){
                        routeParameters = listenableFuture.get();

                        Stop startPoint = new Stop(new Point(startLongitude, startLatitude, SpatialReferences.getWgs84()));
                        Stop endPoint = new Stop(new Point(endLongitude, endLatitude, SpatialReferences.getWgs84()));

                        List<Stop> stops = new ArrayList<>();
                        stops.add(startPoint);
                        stops.add(endPoint);

                        TravelMode travelMode = routeParameters.getTravelMode();
                        travelMode.getRestrictionAttributeNames().clear();
                        travelMode.getRestrictionAttributeNames().add("Preferred for Pedestrians");
                        travelMode.getRestrictionAttributeNames().add("Walking");
                        travelMode.getRestrictionAttributeNames().add("Avoid Toll Roads");
                        travelMode.getRestrictionAttributeNames().add("Roads Under Construction Prohibited");
                        travelMode.getRestrictionAttributeNames().add("Avoid Gates");
                        travelMode.getRestrictionAttributeNames().add("Avoid Express Lanes");
                        travelMode.getRestrictionAttributeNames().add("Avoid Carpool Roads");

//                        travelMode.setName("Walking Distance");
//                        travelMode.setImpedanceAttributeName("WalkTime");
//                        travelMode.setTimeAttributeName("WalkTime");
//                        travelMode.setType("WALK");

                        routeParameters.setStops(stops);
//                        routeParameters.setReturnStops(true);
//                        routeParameters.setReturnDirections(true);
                        routeParameters.setDirectionsStyle(DirectionsStyle.CAMPUS);
                        routeParameters.setFindBestSequence(true);
                        routeParameters.setTravelMode(travelMode);

                        RouteResult routeResult = routeTask.solveRouteAsync(routeParameters).get();
                        final List routeList = routeResult.getRoutes();
                        route = (Route) routeList.get(0);

                        Graphic routeGraphic = new Graphic(route.getRouteGeometry(), lineSymbol);
                        graphicsOverlay.getGraphics().add(routeGraphic);

                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                    }
                }
                catch (Exception e){
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                    Toast.makeText(getApplicationContext(), "Error "+ e.getMessage(), Toast.LENGTH_LONG).show();
                    Log.e(TAG, e.getMessage());
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public static void Detail(Activity activity, String longitude, String latitude){

        Intent intent = new Intent(activity, ArahActivity.class);
        intent.putExtra("longitude", longitude);
        intent.putExtra("latitude", latitude);

        activity.startActivity(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.resume();
    }

    private void setupSymbols() {

        graphicsOverlay = new GraphicsOverlay();

        mapView.getGraphicsOverlays().add(graphicsOverlay);

        Point userLocation = new Point(startLongitude, startLatitude, SpatialReferences.getWgs84());
        Point destination = new Point(endLongitude, endLatitude, SpatialReferences.getWgs84());

        Graphic userGraphic = new Graphic(userLocation, pointMarker("start"));
        Graphic destinationGraphic = new Graphic(destination, pointMarker("end"));

        graphicsOverlay.getGraphics().add(userGraphic);
        graphicsOverlay.getGraphics().add(destinationGraphic);

        lineSymbol = new SimpleLineSymbol(SimpleLineSymbol.Style.SOLID, Color.BLUE, 2);
    }

    private PictureMarkerSymbol pointMarker(String tipe) {
        String url = null;
        String baseUrl = "http://192.168.1.101/";
//        String baseUrl = "http://192.168.43.84/";
        switch (tipe){
            case "start":
                url = baseUrl + "sigapi_rev/public/img/icon/icon_user_location.png";
                break;
            case "end":
                url = baseUrl + "sigapi_rev/public/img/icon/icon_end_location.png";
                break;
        }
        PictureMarkerSymbol pointSymbol = new PictureMarkerSymbol(url);
        pointSymbol.setHeight(32);
        pointSymbol.setWidth(32);
        pointSymbol.loadAsync();

        return pointSymbol;
    }

}
