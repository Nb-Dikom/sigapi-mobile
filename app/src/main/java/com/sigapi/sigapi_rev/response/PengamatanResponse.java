package com.sigapi.sigapi_rev.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sigapi.sigapi_rev.model.Pengamatan;

import java.util.List;

public class PengamatanResponse {
    @SerializedName("code")
    @Expose
    private Integer code;

    @SerializedName("data")
    @Expose
    private List<Pengamatan> data;

    @SerializedName("message")
    @Expose
    private String message;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public List<Pengamatan> getData() {
        return data;
    }

    public void setData(List<Pengamatan> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
