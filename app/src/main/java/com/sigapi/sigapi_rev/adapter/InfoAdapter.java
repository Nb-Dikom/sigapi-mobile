package com.sigapi.sigapi_rev.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sigapi.sigapi_rev.R;
import com.sigapi.sigapi_rev.model.Info;

import java.util.List;

public class InfoAdapter extends RecyclerView.Adapter<InfoAdapter.InfoViewHolder> {
    private List<Info> infos;

    public  InfoAdapter(List<Info> adapter){
        this.infos = adapter;
    }

    public class InfoViewHolder extends RecyclerView.ViewHolder{
        public TextView nama;
        public TextView tgl;

        public InfoViewHolder(LayoutInflater inflater, ViewGroup parent){
            super(inflater.inflate(R.layout.item_info, parent, false));
            nama = (TextView) itemView.findViewById(R.id.nama);
            tgl = (TextView) itemView.findViewById(R.id.tanggal);
        }
    }

    @Override
    public int getItemCount(){
        return infos.size();
    }

    public InfoAdapter.InfoViewHolder onCreateViewHolder(ViewGroup parent, int viewType ){
        return new InfoViewHolder(LayoutInflater.from(parent.getContext()), parent);
    }

    @Override
    public void onBindViewHolder(InfoAdapter.InfoViewHolder holder, int position) {
        holder.nama.setText(infos.get(position).getNamaInfo());
        holder.tgl.setText(infos.get(position).getTglInfo());
    }
}
