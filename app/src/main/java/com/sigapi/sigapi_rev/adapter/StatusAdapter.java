package com.sigapi.sigapi_rev.adapter;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sigapi.sigapi_rev.R;
import com.sigapi.sigapi_rev.model.Status;

import java.util.List;

public class StatusAdapter extends RecyclerView.Adapter<StatusAdapter.StatusViewHolder> {
    private List<Status> statusList;

    public StatusAdapter(List<Status> adapter){
        this.statusList = adapter;
    }

    public class StatusViewHolder extends RecyclerView.ViewHolder{
        public TextView status;
        public View background;
        public RecyclerView mRecyclerViewSecondary;

        public StatusViewHolder(LayoutInflater inflater, ViewGroup parent){
            super(inflater.inflate(R.layout.item_status, parent, false));
            status = (TextView) itemView.findViewById(R.id.status);
            background = (View) itemView.findViewById(R.id.background_status);

            mRecyclerViewSecondary = (RecyclerView) itemView.findViewById(R.id.recycle_view_content);
            mRecyclerViewSecondary.setHasFixedSize(true);

            LinearLayoutManager mLayoutManager = new LinearLayoutManager(parent.getContext());
            mRecyclerViewSecondary.setLayoutManager(mLayoutManager);
        }
    }

    @Override
    public int getItemCount(){
        return statusList.size();
    }

    public StatusAdapter.StatusViewHolder onCreateViewHolder(ViewGroup parent, int viewType ){
        return new StatusViewHolder(LayoutInflater.from(parent.getContext()), parent);
    }

    @Override
    public void onBindViewHolder(StatusAdapter.StatusViewHolder holder, int position) {
        holder.status.setText(statusList.get(position).getStatus());
        switch (statusList.get(position).getStatus()){
            case "AWAS":
                holder.background.setBackgroundResource(R.color.awas);
                break;
            case "SIAGA":
                holder.background.setBackgroundResource(R.color.siaga);
                break;
            case "WASPADA":
                holder.background.setBackgroundResource(R.color.waspada);
                break;
        }
        holder.mRecyclerViewSecondary.setAdapter(new StatusGunungAdapter(statusList.get(position).getStatusGunung()));
    }
}
