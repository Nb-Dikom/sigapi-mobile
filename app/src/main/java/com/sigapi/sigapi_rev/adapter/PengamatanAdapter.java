package com.sigapi.sigapi_rev.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.sigapi.sigapi_rev.R;
import com.sigapi.sigapi_rev.model.Pengamatan;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PengamatanAdapter extends  RecyclerView.Adapter<PengamatanAdapter.PengamatanViewHolder> {
    private List<Pengamatan> pengamatans;
    public Context context;

    public  PengamatanAdapter(List<Pengamatan> adapter, Context context){
        this.pengamatans = adapter;
        this.context = context;
    }

    public class PengamatanViewHolder extends RecyclerView.ViewHolder{
        public ImageView img;

        public PengamatanViewHolder(LayoutInflater inflater, ViewGroup parent){
            super(inflater.inflate(R.layout.item_pengamatan, parent, false));
            img = (ImageView) itemView.findViewById(R.id.foto);
        }
    }
    @Override
    public int getItemCount(){
        return pengamatans.size();
    }

    public PengamatanAdapter.PengamatanViewHolder onCreateViewHolder(ViewGroup parent, int viewType ){
        return new PengamatanAdapter.PengamatanViewHolder(LayoutInflater.from(parent.getContext()), parent);
    }

    @Override
    public void onBindViewHolder(PengamatanAdapter.PengamatanViewHolder holder, int position) {
        if(pengamatans.get(position).getFilePengamatan() == null){
            holder.img.setImageResource(R.drawable.ic_gunung);
        }
        else{
//            String pathImg = "http://192.168.1.101/sigapi_rev/public/upload/pengamatan/" + pengamatans.get(position).getFilePengamatan();
            String pathImg = "http://192.168.43.84/sigapi_rev/public/upload/pengamatan/" + pengamatans.get(position).getFilePengamatan();
//            String pathImg = "http://192.168.0.107/sigapi_rev/public/upload/pengamatan/" + pengamatans.get(position).getFilePengamatan();
            Picasso.with(context)
                    .load(pathImg)
                    .error(R.drawable.noimagefound)
                    .placeholder(R.drawable.ic_gunung)
                    .fit()
                    .into(holder.img);
        }
    }
}
