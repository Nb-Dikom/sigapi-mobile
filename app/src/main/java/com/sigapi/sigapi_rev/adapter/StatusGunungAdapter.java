package com.sigapi.sigapi_rev.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sigapi.sigapi_rev.R;
import com.sigapi.sigapi_rev.model.StatusGunung;

import java.util.List;

public class StatusGunungAdapter extends RecyclerView.Adapter<StatusGunungAdapter.StatusViewHolder> {
    private List<StatusGunung> statusGunungList;

    public StatusGunungAdapter(List<StatusGunung> adapter){
        this.statusGunungList = adapter;
    }

    public class StatusViewHolder extends RecyclerView.ViewHolder{
        public TextView gunung;
        public TextView tanggal;

        public StatusViewHolder(LayoutInflater inflater, ViewGroup parent){
            super(inflater.inflate(R.layout.item_status_gunung, parent, false));
            gunung = (TextView) itemView.findViewById(R.id.gunung);
            tanggal = (TextView) itemView.findViewById(R.id.tanggal);
        }
    }

    @Override
    public int getItemCount(){
        return statusGunungList.size();
    }

    public StatusGunungAdapter.StatusViewHolder onCreateViewHolder(ViewGroup parent, int viewType ){
        return new StatusViewHolder(LayoutInflater.from(parent.getContext()), parent);
    }

    @Override
    public void onBindViewHolder(StatusGunungAdapter.StatusViewHolder holder, int position) {
        holder.gunung.setText(statusGunungList.get(position).getGunung());
        holder.tanggal.setText(statusGunungList.get(position).getTanggal());
    }
}
