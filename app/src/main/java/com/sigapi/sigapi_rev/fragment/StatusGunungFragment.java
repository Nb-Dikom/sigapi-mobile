package com.sigapi.sigapi_rev.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.sigapi.sigapi_rev.R;
import com.sigapi.sigapi_rev.adapter.StatusAdapter;
import com.sigapi.sigapi_rev.api.ApiInterface;
import com.sigapi.sigapi_rev.model.Status;
import com.sigapi.sigapi_rev.response.StatusGunungResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class StatusGunungFragment extends Fragment {
    private static final String TAG = "";
    public static final String BASE_URL = "http://192.168.1.101/sigapi/public/api/";
//    public static final String BASE_URL = "http://192.168.43.84/sigapi/public/api/";
//    public static final String BASE_URL = "http://192.168.0.107/sigapi/public/api/";
    private List<Status> statusList;
    private RecyclerView mRecyclerView;
    private TextView error;

    public static StatusGunungFragment newInstance(){
        return new StatusGunungFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_status_gunung, container, false);

        Toolbar mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
        mToolbar.setTitle(R.string.app_name);

        error = (TextView) view.findViewById(R.id.text_error);
        error.setVisibility(View.GONE);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycle_view);
        mRecyclerView.setHasFixedSize(true);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this.getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        getStatus();
        return  view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getStatus();
    }

    private void getStatus(){
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Harap tunggu ...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<StatusGunungResponse> call = service.getListStatusGunung();
        call.enqueue(new Callback<StatusGunungResponse>() {
            @Override
            public void onResponse(Call<StatusGunungResponse> call, Response<StatusGunungResponse> response) {
                try {
                    progressDialog.dismiss();

                    if(response.body().getCode() == 1){
                        statusList = response.body().getData();

                        if(statusList.size() > 0){
                            mRecyclerView.setAdapter(new StatusAdapter(statusList));
                        }
                        else{
                            mRecyclerView.setVisibility(View.GONE);
                            error.setVisibility(View.VISIBLE);
                            error.setText(R.string.text_error);
                        }
                    }
                    else if(response.body().getCode() == 2){
                        mRecyclerView.setVisibility(View.GONE);
                        error.setVisibility(View.VISIBLE);
                        error.setText(R.string.text_error);
                        Toast.makeText(getContext(), "Data tidak ditemukan"+response.body().getCode(),Toast.LENGTH_SHORT).show();
                        Log.e(TAG, "Gagal :"+ response.body().getMessage());
                    }

                }catch (Exception e){
                    progressDialog.dismiss();

                    mRecyclerView.setVisibility(View.GONE);
                    error.setVisibility(View.VISIBLE);
                    error.setText("Terjadi kesalahan");
                    Toast.makeText(getContext(), "Terjadi kesalahan",Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "Gagal :"+e);
                }
            }

            @Override
            public void onFailure(Call<StatusGunungResponse> call, Throwable t) {
                progressDialog.dismiss();

                mRecyclerView.setVisibility(View.GONE);
                error.setVisibility(View.VISIBLE);
                error.setText(R.string.text_error);
                Toast.makeText(getContext(), "Data tidak ditemukan",Toast.LENGTH_SHORT).show();
                Log.e(TAG, "Gagal :"+t.toString());
            }
        });
    }

    public static interface ClickListener{
        public void onClick(View view, int position);
        public void onLongClick(View view, int position);
    }

    class RecyclerTouchListener implements RecyclerView.OnItemTouchListener{

        private ClickListener clicklistener;
        private GestureDetector gestureDetector;

        public RecyclerTouchListener(Context context, final RecyclerView recycleView, final ClickListener clicklistener){

            this.clicklistener=clicklistener;
            gestureDetector=new GestureDetector(context,new GestureDetector.SimpleOnGestureListener(){
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child=recycleView.findChildViewUnder(e.getX(),e.getY());
                    if(child!=null && clicklistener!=null){
                        clicklistener.onLongClick(child,recycleView.getChildAdapterPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child=rv.findChildViewUnder(e.getX(),e.getY());
            if(child!=null && clicklistener!=null && gestureDetector.onTouchEvent(e)){
                clicklistener.onClick(child,rv.getChildAdapterPosition(child));
            }

            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
