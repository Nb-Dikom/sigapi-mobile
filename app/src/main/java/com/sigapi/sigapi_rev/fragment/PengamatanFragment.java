package com.sigapi.sigapi_rev.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.DimenRes;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.sigapi.sigapi_rev.R;
import com.sigapi.sigapi_rev.activity.DetailPengamatanActivity;
import com.sigapi.sigapi_rev.adapter.PengamatanAdapter;
import com.sigapi.sigapi_rev.api.ApiInterface;
import com.sigapi.sigapi_rev.model.Pengamatan;
import com.sigapi.sigapi_rev.response.PengamatanResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PengamatanFragment extends Fragment {
    private static final String TAG = "";
    public static final String BASE_URL = "http://192.168.1.101/sigapi/public/api/";
//    public static final String BASE_URL = "http://192.168.43.84/sigapi/public/api/";
//    public static final String BASE_URL = "http://192.168.0.107/sigapi/public/api/";
    private List<Pengamatan> pengamatans;
    private RecyclerView mRecyclerView;
    private TextView error;

    public static PengamatanFragment newInstance(){
        return new PengamatanFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_pengamatan, container, false);

        Toolbar mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
        mToolbar.setTitle(R.string.app_name);

        error = (TextView) view.findViewById(R.id.text_error);
        error.setVisibility(View.GONE);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycle_view);
        mRecyclerView.setHasFixedSize(true);

        GridLayoutManager mLayoutManager = new GridLayoutManager(this.getActivity(), 3);
        mRecyclerView.setLayoutManager(mLayoutManager);

        ItemOffsetDecoration itemOffsetDecoration = new ItemOffsetDecoration(this.getActivity(), R.dimen.grid_margin);
        mRecyclerView.addItemDecoration(itemOffsetDecoration);

        getPengamatan();
        mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), mRecyclerView, new ClickListener() {
            @Override
            public void onClick(View view, final int position){
                DetailPengamatanActivity.Detail(getActivity(),
                        pengamatans.get(position).getIdPengamatan(),
                        pengamatans.get(position).getNamaPengamatan(),
                        pengamatans.get(position).getFilePengamatan(),
                        pengamatans.get(position).getTglPengamatan());
            }
            @Override
            public void onLongClick(View view, int position){

            }
        }));
        return  view;
    }

    private void getPengamatan(){
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Harap tunggu ...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<PengamatanResponse> call = service.getListPengamatan();
        call.enqueue(new Callback<PengamatanResponse>() {
            @Override
            public void onResponse(Call<PengamatanResponse> call, Response<PengamatanResponse> response) {
                try {
                    progressDialog.dismiss();

                    if(response.body().getCode() == 1){
                        pengamatans = response.body().getData();

                        if(pengamatans.size() > 0){
                            mRecyclerView.setAdapter(new PengamatanAdapter(pengamatans, getContext()));
                        }
                        else{
                            mRecyclerView.setVisibility(View.GONE);
                            error.setVisibility(View.VISIBLE);
                            error.setText(R.string.text_error);
                        }
                    }
                    else if(response.body().getCode() == 2){
                        mRecyclerView.setVisibility(View.GONE);
                        error.setVisibility(View.VISIBLE);
                        error.setText(R.string.text_error);
                        Toast.makeText(getContext(), "Data tidak ditemukan"+response.body().getCode(),Toast.LENGTH_SHORT).show();
                        Log.e(TAG, "Gagal :"+ response.body().getMessage());
                    }

                }catch (Exception e){
                    progressDialog.dismiss();

                    mRecyclerView.setVisibility(View.GONE);
                    error.setVisibility(View.VISIBLE);
                    error.setText("Terjadi kesalahan");
                    Toast.makeText(getContext(), "Terjadi kesalahan",Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "Gagal :"+e);
                }
            }

            @Override
            public void onFailure(Call<PengamatanResponse> call, Throwable t) {
                progressDialog.dismiss();

                mRecyclerView.setVisibility(View.GONE);
                error.setVisibility(View.VISIBLE);
                error.setText(R.string.text_error);
                Toast.makeText(getContext(), "Data tidak ditemukan",Toast.LENGTH_SHORT).show();
                Log.e(TAG, "Gagal :"+t.toString());
            }
        });
    }

    public class ItemOffsetDecoration extends RecyclerView.ItemDecoration {

        private int mItemOffset;

        public ItemOffsetDecoration(int itemOffset) {
            mItemOffset = itemOffset;
        }

        public ItemOffsetDecoration(@NonNull Context context, @DimenRes int itemOffsetId) {
            this(context.getResources().getDimensionPixelSize(itemOffsetId));
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);
            outRect.set(mItemOffset, mItemOffset, mItemOffset, mItemOffset);
        }
    }

    public static interface ClickListener{
        public void onClick(View view,int position);
        public void onLongClick(View view,int position);
    }

    class RecyclerTouchListener implements RecyclerView.OnItemTouchListener{

        private ClickListener clicklistener;
        private GestureDetector gestureDetector;

        public RecyclerTouchListener(Context context, final RecyclerView recycleView, final ClickListener clicklistener){

            this.clicklistener=clicklistener;
            gestureDetector=new GestureDetector(context,new GestureDetector.SimpleOnGestureListener(){
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child=recycleView.findChildViewUnder(e.getX(),e.getY());
                    if(child!=null && clicklistener!=null){
                        clicklistener.onLongClick(child,recycleView.getChildAdapterPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child=rv.findChildViewUnder(e.getX(),e.getY());
            if(child!=null && clicklistener!=null && gestureDetector.onTouchEvent(e)){
                clicklistener.onClick(child,rv.getChildAdapterPosition(child));
            }

            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
