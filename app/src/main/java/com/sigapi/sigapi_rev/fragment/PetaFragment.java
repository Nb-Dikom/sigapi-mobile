package com.sigapi.sigapi_rev.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.esri.arcgisruntime.ArcGISRuntimeEnvironment;
import com.esri.arcgisruntime.concurrent.ListenableFuture;
import com.esri.arcgisruntime.data.Field;
import com.esri.arcgisruntime.data.ServiceFeatureTable;
import com.esri.arcgisruntime.geometry.Point;
import com.esri.arcgisruntime.geometry.SpatialReference;
import com.esri.arcgisruntime.geometry.SpatialReferences;
import com.esri.arcgisruntime.layers.FeatureLayer;
import com.esri.arcgisruntime.loadable.LoadStatus;
import com.esri.arcgisruntime.mapping.ArcGISMap;
import com.esri.arcgisruntime.mapping.Basemap;
import com.esri.arcgisruntime.mapping.view.Callout;
import com.esri.arcgisruntime.mapping.view.DefaultMapViewOnTouchListener;
import com.esri.arcgisruntime.mapping.view.Graphic;
import com.esri.arcgisruntime.mapping.view.GraphicsOverlay;
import com.esri.arcgisruntime.mapping.view.IdentifyGraphicsOverlayResult;
import com.esri.arcgisruntime.mapping.view.MapView;
import com.esri.arcgisruntime.symbology.PictureMarkerSymbol;
import com.esri.arcgisruntime.symbology.SimpleFillSymbol;
import com.esri.arcgisruntime.symbology.SimpleRenderer;
import com.esri.arcgisruntime.util.ListenableList;
import com.sigapi.sigapi_rev.R;
import com.sigapi.sigapi_rev.activity.ArahActivity;
import com.sigapi.sigapi_rev.api.ApiInterface;
import com.sigapi.sigapi_rev.model.Fasilitas;
import com.sigapi.sigapi_rev.model.Krb;
import com.sigapi.sigapi_rev.response.PetaResponse;
import com.sigapi.sigapi_rev.util.GPSTracker;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PetaFragment extends Fragment {
    public static final String BASE_URL = "http://192.168.1.101/sigapi/public/";
//    public static final String BASE_URL = "http://192.168.43.84/sigapi/public/";
//    public static final String BASE_URL = "http://192.168.0.107/sigapi/public/";
    private static final String TAG = "LOG";
    private ProgressDialog progressDialog;
    private FloatingActionButton fab;
    private List<Fasilitas> fasilitasList;
    private List<Krb> krbList;

    private ArcGISMap mMap;
    private MapView mMapView;
    private GraphicsOverlay pointGraphicOverlay;
    private Callout mCallout;
    private double bujurTimur, lintangSelatan;

    private Field fields;

    public static PetaFragment newInstance(){
        return new PetaFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_peta, container, false);

        Toolbar mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
        mToolbar.setTitle(R.string.app_name);

        progressDialog = new ProgressDialog(getActivity());
        mMapView = (MapView) view.findViewById(R.id.mapView);
        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setVisibility(View.GONE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArahActivity.Detail(getActivity(), String.valueOf(bujurTimur), String.valueOf(lintangSelatan));
            }
        });

        getPeta();
        return  view;
    }

    @Override
    public void onResume(){
        super.onResume();
        mMapView.resume();
    }

    @Override
    public void onPause(){
        super.onPause();
        mMapView.pause();
    }

    private void getPeta(){
        progressDialog.setMessage("Harap tunggu ...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL + "api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<PetaResponse> call = service.getListPeta();
        call.enqueue(new Callback<PetaResponse>() {
            @Override
            public void onResponse(Call<PetaResponse> call, Response<PetaResponse> response) {
                try {
                    progressDialog.dismiss();
                    if(response.body().getCode() == 1){
                        fasilitasList = response.body().getData().getFasilitas();
                        krbList = response.body().getData().getKrb();
                        setMap(krbList, fasilitasList);
                    }
                    else if(response.body().getCode() == 2){
                        Toast.makeText(getContext(), "Data tidak ditemukan"+response.body().getCode(),Toast.LENGTH_SHORT).show();
                        Log.e(TAG, "Data tidak ditemukan: "+ response.body().getMessage());
                    }

                }catch (Exception e){
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), "Error response",Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "Error response: "+e);
                }
            }

            @Override
            public void onFailure(Call<PetaResponse> call, Throwable t) {
                progressDialog.dismiss();

                Toast.makeText(getContext(), "Terjadi Kesalahan",Toast.LENGTH_SHORT).show();
                Log.e(TAG, "Gagal :"+t.toString());
            }
        });
    }

    private void setMap(List<Krb> daftarKrb ,List<Fasilitas> daftarFasilitas){
        ArcGISRuntimeEnvironment.setLicense("runtimelite,1000,rud6300666834,none,6PB3LNBHPBHDLMZ59175");
        mMap = new ArcGISMap(Basemap.Type.IMAGERY_WITH_LABELS_VECTOR, -8.2389649, 113.0205131, 11);
//        fields.createString("nama_fasilitas", "NAMA", 64);
//        String url = "http://services7.arcgis.com/SyInLC41qCR7fZ9v/arcgis/rest/services/lokasi_pengungsian/FeatureServer/0";
//        ServiceFeatureTable serviceFeatureTable = new ServiceFeatureTable(url);
//        FeatureLayer featureLayer = new FeatureLayer(serviceFeatureTable);
//        mMap.getOperationalLayers().add(featureLayer);

        mMapView.setMap(mMap);

        mCallout = mMapView.getCallout();
        MapViewTouchListener mMapViewTouchListener = new MapViewTouchListener(getActivity(), mMapView);
        mMapView.setOnTouchListener(mMapViewTouchListener);

        addKrb(daftarKrb);
        addPoint(daftarFasilitas);

//        mCallout = mMapView.getCallout();
//        mMapView.setOnTouchListener(new DefaultMapViewOnTouchListener(getActivity(), mMapView) {
//            @Override
//            public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
//                if(mCallout.isShowing()){
//                    mCallout.dismiss();
//                }
//
//                android.graphics.Point screenPoint = new android.graphics.Point(Math.round(motionEvent.getX()),
//                        Math.round(motionEvent.getY()));
//
//                Point mapPoint = mMapView.screenToLocation(screenPoint);
//                Point wgs84Point = (Point) GeometryEngine.project(mapPoint, SpatialReferences.getWgs84());
//
//                TextView calloutContent = new TextView(getActivity());
//                calloutContent.setTextColor(Color.BLACK);
//                calloutContent.setSingleLine(false);
//                calloutContent.setVerticalScrollBarEnabled(true);
//                calloutContent.setScrollBarStyle(View.SCROLLBARS_INSIDE_INSET);
//                calloutContent.setMovementMethod(new ScrollingMovementMethod());
//                calloutContent.setLines(5);
//
//                calloutContent.setText("Lat: " +  String.format("%.4f", wgs84Point.getY()) +
//                        ", Lon: " + String.format("%.4f", wgs84Point.getX()));
//
//                mCallout = mMapView.getCallout();
//                mCallout.setLocation(mapPoint);
//                mCallout.setContent(calloutContent);
//                mCallout.show();
//
//                mMapView.setViewpointCenterAsync(mapPoint);
//                return true;
//            }
//        });
    }

    private void addKrb(List<Krb> daftarKrb) {
        try {
//            Portal portal = new Portal("http://www.arcgis.com");
//            PortalItem portalItem = new PortalItem(portal, "338a2fad82bb401aaae81b3b96e3362b");
//            FeatureLayer featureLayer = new FeatureLayer(portalItem,0);
            for (int index = 0; index < daftarKrb.size(); index++) {

                SimpleFillSymbol polygonSymbol = setFill(daftarKrb.get(index).getIdJenisKrb());
                SimpleRenderer renderer = new SimpleRenderer();
                renderer.setSymbol(polygonSymbol);

                ServiceFeatureTable serviceFeatureTable = new ServiceFeatureTable(daftarKrb.get(index).getFileKrb());
                FeatureLayer featureLayer = new FeatureLayer(serviceFeatureTable);
                featureLayer.setRenderer(renderer);
                featureLayer.addDoneLoadingListener(() -> {
                    if (featureLayer.getLoadStatus() == LoadStatus.LOADED) {
                        progressDialog.dismiss();
                    }
                });

                mMap.getOperationalLayers().add(featureLayer);
            }
        }
        catch (Exception e){
            Log.e(TAG, "Gagal memuat krb: "+e.getMessage());
        }
    }

    private void addPoint(List<Fasilitas> daftarFasilitas){
        SpatialReference SPATIAL_REFERENCE = SpatialReferences.getWgs84();
        pointGraphicOverlay = new GraphicsOverlay();
        ListenableList<Graphic> graphics = pointGraphicOverlay.getGraphics();

        for (int index = 0; index < daftarFasilitas.size(); index++){
            double bujurTimur = Double.parseDouble(daftarFasilitas.get(index).getLongitudeFasilitas());
            double lintangSelatan = Double.parseDouble(daftarFasilitas.get(index).getLatitudeFasilitas());
            int idJenisFasilitas = daftarFasilitas.get(index).getIdJenisFasilitas();

            PictureMarkerSymbol pointSymbol = new PictureMarkerSymbol(setMarker(idJenisFasilitas));
            pointSymbol.setHeight(18);
            pointSymbol.setWidth(18);
            pointSymbol.loadAsync();

            Map<String, Object> attributes = new HashMap<>();
            attributes.put("nama_fasilitas", daftarFasilitas.get(index).getNamaFasilitas());
            attributes.put("alamat_fasilitas", daftarFasilitas.get(index).getAlamatFasilitas());
            attributes.put("longitude", daftarFasilitas.get(index).getLongitudeFasilitas());
            attributes.put("latitude", daftarFasilitas.get(index).getLatitudeFasilitas());
            attributes.put("jenis_fasilitas", daftarFasilitas.get(index).getJenisFasilitas());
            attributes.put("gunung", daftarFasilitas.get(index).getGunung());

            Point point = new Point(bujurTimur, lintangSelatan, SPATIAL_REFERENCE);

            Graphic pointGraphic = new Graphic(point, attributes, pointSymbol);
            graphics.add(pointGraphic);
        }

        GPSTracker gpsTracker = new GPSTracker(getActivity());
        Double startLongitude = gpsTracker.getLongitude();
        Double startLatitude = gpsTracker.getLatitude();
        PictureMarkerSymbol pointSymbol = new PictureMarkerSymbol(setMarker(13));
        pointSymbol.setHeight(24);
        pointSymbol.setWidth(24);
        pointSymbol.loadAsync();

        Point point = new Point(startLongitude, startLatitude, SPATIAL_REFERENCE);
        Graphic pointGraphic = new Graphic(point, pointSymbol);
        graphics.add(pointGraphic);

        mMapView.getGraphicsOverlays().add(pointGraphicOverlay);
    }

    class MapViewTouchListener extends DefaultMapViewOnTouchListener{
        public MapViewTouchListener(Context context, MapView mapView){
            super(context, mapView);
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
            if(mCallout.isShowing()){
                mCallout.dismiss();
                fab.setVisibility(View.GONE);
            }

            android.graphics.Point screenPoint = new android.graphics.Point(Math.round(motionEvent.getX()),
                    Math.round(motionEvent.getY()));

            final SpatialReference spatialReference = SpatialReferences.getWgs84();
            final ListenableFuture<IdentifyGraphicsOverlayResult> identifyPointGraphic = mMapView.identifyGraphicsOverlayAsync(pointGraphicOverlay, screenPoint, 10.0, false, 2);

            identifyPointGraphic.addDoneListener(new Runnable() {
                @Override
                public void run() {
                    try {
                        IdentifyGraphicsOverlayResult graphicsOverlayResult = identifyPointGraphic.get();

                        Iterator<Graphic> iterator = graphicsOverlayResult.getGraphics().iterator();

                        Graphic graphic = null;
                        while (iterator.hasNext()){
                            graphic = iterator.next();
                            String setText = "Nama: "+ graphic.getAttributes().get("nama_fasilitas") + "\n" +
                                    "Alamat: " + graphic.getAttributes().get("alamat_fasilitas") + "\n" +
                                    "Jenis: " + graphic.getAttributes().get("jenis_fasilitas") + "\n" +
                                    "Gunung: " + graphic.getAttributes().get("gunung") + "\n";

                            bujurTimur = Double.parseDouble(graphic.getAttributes().get("longitude").toString());
                            lintangSelatan = Double.parseDouble(graphic.getAttributes().get("latitude").toString());

                            Point mapPoint = new Point(bujurTimur, lintangSelatan, spatialReference);

                            TextView calloutContent = new TextView(getActivity());
                            calloutContent.setTextColor(Color.BLACK);
                            calloutContent.setSingleLine(false);
                            calloutContent.setVerticalScrollBarEnabled(true);
                            calloutContent.setScrollBarStyle(View.SCROLLBARS_INSIDE_INSET);
                            calloutContent.setMovementMethod(new ScrollingMovementMethod());
                            calloutContent.setText(setText);

                            mCallout = mMapView.getCallout();
                            mCallout.setLocation(mapPoint);
                            mCallout.setContent(calloutContent);
                            mCallout.show();

                            mMapView.setViewpointCenterAsync(mapPoint);
                            fab.setVisibility(View.VISIBLE);
                        }

                    }catch(InterruptedException | ExecutionException ie){
                        ie.printStackTrace();
                    }
                }
            });
            return super.onSingleTapConfirmed(motionEvent);
        }
    }

    private String setMarker(int idJenisFasilitas){
        String pointMarker = null;
        switch (idJenisFasilitas){
            case 1:
                pointMarker = BASE_URL + "img/icon/Kantor Polisi.png";
                break;
            case 2:
                pointMarker = BASE_URL + "img/icon/Kantor Pemadam Kebakaran.png";
                break;
            case 3:
                pointMarker = BASE_URL + "img/icon/Kantor Pemerintahan.png";
                break;
            case 4:
                pointMarker = BASE_URL + "img/icon/Pertokoan.png";
                break;
            case 5:
                pointMarker = BASE_URL + "img/icon/Pusat Perbelanjaan.png";
                break;
            case 6:
                pointMarker = BASE_URL + "img/icon/Pasar Tradisional.png";
                break;
            case 7:
                pointMarker = BASE_URL + "img/icon/Tempat Rekreasi (Buatan dan Alami di Pegunungan).png";
                break;
            case 8:
                pointMarker = BASE_URL + "img/icon/Terminal Bis.png";
                break;
            case 9:
                pointMarker = BASE_URL + "img/icon/Stasiun Kereta Api.png";
                break;
            case 10:
                pointMarker = BASE_URL + "img/icon/Posko (Rupusdalops).png";
                break;
            case 11:
                pointMarker = BASE_URL + "img/icon/Rumah Sakit.png";
                break;
            case 12:
                pointMarker = BASE_URL + "img/icon/Puskesmas.png";
                break;
            case 13:
                pointMarker = BASE_URL + "img/icon/icon_user_location.png";
                break;
        }
        return pointMarker;
    }

    private SimpleFillSymbol setFill(int idJenisKrb){
        SimpleFillSymbol fillSymbol = null;
        switch (idJenisKrb){
            case 1:
                fillSymbol = new SimpleFillSymbol(SimpleFillSymbol.Style.SOLID, getResources().getColor(R.color.krb1), null);
                break;
            case 2:
                fillSymbol = new SimpleFillSymbol(SimpleFillSymbol.Style.SOLID, getResources().getColor(R.color.krb2), null);
                break;
            case 3:
                fillSymbol = new SimpleFillSymbol(SimpleFillSymbol.Style.SOLID, getResources().getColor(R.color.krb3), null);
                break;
        }
        return fillSymbol;
    }
}
