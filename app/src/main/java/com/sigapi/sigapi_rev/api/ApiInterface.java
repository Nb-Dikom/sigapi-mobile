package com.sigapi.sigapi_rev.api;

import com.sigapi.sigapi_rev.response.InfoResponse;
import com.sigapi.sigapi_rev.response.PengamatanResponse;
import com.sigapi.sigapi_rev.response.PetaResponse;
import com.sigapi.sigapi_rev.response.StatusGunungResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {
    @GET("status-gunung")
    Call<StatusGunungResponse> getListStatusGunung();

    @GET("info")
    Call<InfoResponse> getListInfo();

    @GET("pengamatan")
    Call<PengamatanResponse> getListPengamatan();

    @GET("peta")
    Call<PetaResponse> getListPeta();
}
